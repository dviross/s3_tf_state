#!/bin/bash
set -eu

pushd tf/
terraform apply --auto-approve
popd