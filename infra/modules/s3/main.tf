
#S3 state bucket--
resource "aws_s3_bucket" "tf-state" {
  # count         = 1
  bucket        = var.state_bucket_name
  force_destroy = true //to delete even thogh state files still reiside inside
}

resource "aws_s3_bucket_ownership_controls" "s3_bucket_acl_ownership" {
  bucket = aws_s3_bucket.tf-state.id
  rule {
    object_ownership = "ObjectWriter"
  }
}

resource "aws_s3_bucket_acl" "tf-state_bucket_acl" {
  bucket = aws_s3_bucket.tf-state.id
  acl    = "private"
depends_on = [aws_s3_bucket_ownership_controls.s3_bucket_acl_ownership]

}

resource "aws_s3_bucket_versioning" "tf-state_bucket_ver" {
  bucket = aws_s3_bucket.tf-state.id
  versioning_configuration {
    status = "Enabled"
  }
}

resource "aws_s3_bucket_server_side_encryption_configuration" "tf-state_bucket_sse_config" {
  bucket = aws_s3_bucket.tf-state.bucket

  rule {
    apply_server_side_encryption_by_default {
      kms_master_key_id = var.kms_master_key_id
      sse_algorithm     = "aws:kms"
      # sse_algorithm = "AES256"
    }
  }
}

#public access block to tf state--
resource "aws_s3_bucket_public_access_block" "block" {
  bucket = aws_s3_bucket.tf-state.id

  block_public_acls       = true
  block_public_policy     = true
  ignore_public_acls      = true
  restrict_public_buckets = true
}

# resource "aws_s3_bucket_acl" "tfstate" {
#   bucket = aws_s3_bucket.tfstate.id
#   acl    = "private"
# }
