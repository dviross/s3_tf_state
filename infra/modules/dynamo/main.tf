resource "aws_dynamodb_table" "tf-state" {
  name           = "tf-state"
  read_capacity  = 20
  write_capacity = 20
  hash_key       = "LockID"

  attribute {
    name = "LockID"
    type = "S"
  }
}