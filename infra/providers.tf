
terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "4.62.0"
    }
    # tls = {
    #   source  = "hashicorp/tls"
    #   version = "4.0.4"
    # }
  }
}

provider "aws" {
  region  = var.region
  profile = var.profile #"default"
}