
profile = "default"
region  = "eu-central-1" #frankfurt

tags = {
  Owner           = "Dvir Gross"
  expiration_date = "x.x.x"
  bootcamp        = "16"
}

state_bucket_name = "dvir-tf-state"