
output "enabled" {
  value       = module.s3.enabled
  description = "Is module enabled"
}

output "bucket_id" {
  value       = module.s3.bucket_id
  description = "Bucket Name (aka ID)"
}

output "bucket_arn" {
  value       = module.s3.bucket_arn
  description = "Bucket ARN"
}

output "bucket_region" {
  value       = module.s3.bucket_region
  description = "Bucket region"
}

output "bucket_domain_name" {
  value       = module.s3.bucket_domain_name
  description = "FQDN of bucket"
}

output "bucket_regional_domain_name" {
  value       = module.s3.bucket_regional_domain_name
  description = "The bucket region-specific domain name"
}

# output "bucket_id" {
#   value = module.s3.bucket_id
#   description = "Bucket Name (aka ID)"
# }

