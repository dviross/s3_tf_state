

# state ->
module "s3" {
  source            = "./modules/s3"
  kms_master_key_id = module.kms.kms_key.arn
  state_bucket_name = var.state_bucket_name
}

module "kms" {
  source = "./modules/kms"
}

module "dynamo" {
  source = "./modules/dynamo"
}

# module "s3_bucket" {
#   source = "terraform-aws-modules/s3-bucket/aws"

#   bucket = "dvir-tf-state"
#   acl    = "private"

#   versioning = {
#     enabled = true
#   }

# }

# module "dynamodb_table" {
#   source   = "terraform-aws-modules/dynamodb-table/aws"

#   name     = "dvir-tf-state"
#   hash_key = "id"

#   attributes = [
#     {
#       name = "id"
#       type = "N"
#     }
#   ]

#   tags = {
#     Terraform   = "true"
#     Environment = "staging"
#   }
# }
